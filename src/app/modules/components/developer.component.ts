import { Component, OnInit, TemplateRef } from '@angular/core';
import { Developer } from '../modal/developer';

@Component({
  selector: 'app-developer',
  templateUrl: './developer.component.html',
  styleUrls: ['./developer.component.scss'],
})
export class DeveloperComponent implements OnInit {
  cardList: Developer[] = [
    {
      name: 'Sachet',
      date1: 1,
      date2: 1,
      date3: 1,
      date4: 1,
      date5: 1,
      date6: 1,
      date7: 1,
      total: 7,
    },
    {
      name: 'Miken',
      date1: 1,
      date2: 1,
      date3: 1,
      date4: 1,
      date5: 1,
      date6: 1,
      date7: 1,
      total: 7,
    },
    {
      name: 'Devfinity',
      date1: 1,
      date2: 1,
      date3: 1,
      date4: 1,
      date5: 1,
      date6: 1,
      date7: 1,
      total: 7,
    },
    {
      name: 'Manoj',
      date1: 1,
      date2: 1,
      date3: 1,
      date4: 1,
      date5: 1,
      date6: 1,
      date7: 1,
      total: 7,
    },
    {
      name: 'Sai',
      date1: 1,
      date2: 1,
      date3: 1,
      date4: 1,
      date5: 1,
      date6: 1,
      date7: 1,
      total: 7,
    },
    {
      name: 'Santosh',
      date1: 1,
      date2: 1,
      date3: 1,
      date4: 1,
      date5: 1,
      date6: 1,
      date7: 1,
      total: 7,
    },
    {
      name: 'Grafi',
      date1: 1,
      date2: 1,
      date3: 1,
      date4: 1,
      date5: 1,
      date6: 1,
      date7: 1,
      total: 7,
    },
  ];
  closeResult: string = '';

  constructor() {}

  ngOnInit(): void {}

  onCancel(): void {}

  onSave() {}

  totalHours(item: any, i: any): void {
    console.log(item, i, 'miken');
    this.cardList[i].total =
      item.date1 +
      item.date2 +
      item.date3 +
      item.date4 +
      item.date5 +
      item.date6 +
      item.date7;
  }
}
