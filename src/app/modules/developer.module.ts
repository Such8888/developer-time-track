import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeveloperRoutingModule } from './developer-routing.module';
import { DeveloperComponent } from './components/developer.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [DeveloperComponent],
  imports: [CommonModule, DeveloperRoutingModule, FormsModule],
})
export class DeveloperModule {}
